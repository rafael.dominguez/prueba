# prueba

a [Sails v1](https://sailsjs.com) application


### Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)


### Version info

Se ocupo como framework sails y cada funcion esta en un diferente archivo de tipo controller. Anexo como se debe de invocar cada uno

Par instalar primeramente ejecutar la instruccion: npm install

+ http://localhost:1337/funcion2
+ http://localhost:1337/funcion3
+ http://localhost:1337/funcion4
+ http://localhost:1337/funcion5



<!-- Internally, Sails used [`sails-generate@1.16.13`](https://github.com/balderdashy/sails-generate/tree/v1.16.13/lib/core-generators/new). -->



<!--
Note:  Generators are usually run using the globally-installed `sails` CLI (command-line interface).  This CLI version is _environment-specific_ rather than app-specific, thus over time, as a project's dependencies are upgraded or the project is worked on by different developers on different computers using different versions of Node.js, the Sails dependency in its package.json file may differ from the globally-installed Sails CLI release it was originally generated with.  (Be sure to always check out the relevant [upgrading guides](https://sailsjs.com/upgrading) before upgrading the version of Sails used by your app.  If you're stuck, [get help here](https://sailsjs.com/support).)
-->

+ Ejercicio 2:Este ejercicio su complejidad fue baja, se tuvo que analizar el ejemplo de entrada y que salida asi como la formula proporcionada. el tiempo aproximado fue de 15 min de desarrollo.
+ Ejercicio 3: Este ejercicio su complejidad fue baja, se tuvo que analizar el ejemplo de entrada y que salida . el tiempo aproximado fue de 20 min de desarrollo
+ Ejercicio 4: Este ejercicio su complejidad fue baja, se tuvo que analizar el ejemplo de entrada y que salida . el tiempo aproximado fue de 20 min de desarrollo actualmente con javascript y ES se puede hacer en una sola linea pero para efecto tambien de esta prueba realice el eliminado de duplicados con un pequeño ciclo
+ Ejercicio 5:Este ejercicio su complejidad fue media, el tiempo aqui fue de 2 horas. 

+ Se realizo un script para ponerlo en POSTMAN (PRE-REQUEST SCRIPT)

/*FUNCION2 */
var m =  Math.floor(Math.random()*11);
var n =  Math.floor(Math.random() * (m-1)+1);
postman.setEnvironmentVariable('NUMBER_N',n);
postman.setEnvironmentVariable('NUMBER_M',m);
/*FUNCION3 */

var arreglo=[];
var size_array=Math.floor(Math.random()*11);
for(var i=0;i<size_array;i++){
    arreglo[i]=Math.floor(Math.random()*101);
}
var value =Math.floor(Math.random()*size_array)
postman.setEnvironmentVariable('ARRAY_1',JSON.stringify(arreglo));
postman.setEnvironmentVariable('VALUE_K',value);


/*FUNCION4 */

var arreglo2=[];
var size_array2=Math.floor(Math.random()*21);
for(var i=0;i<size_array2;i++){
    arreglo2[i]=Math.floor(Math.random()*11);
}
postman.setEnvironmentVariable('ARRAY_2',JSON.stringify(arreglo2));



/*FUNCION5 */
var characters       = '[]()';
var charactersLength = characters.length;
var size2= Math.floor(Math.random()*21);
var result='';
for ( var i = 0; i < size2; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
 }
postman.setEnvironmentVariable('ARRAY_3',result);




