/**
 * Funcion5Controller
 *
 * @description :: Este ejercicio su complejidad fue media, el tiempo aqui fue de 2 horas.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    balanced: async function (req, res) {
        var values = req.allParams();
        let cadena = values.cadena
        if (cadena === undefined && cadena.length>0) {
            return res.status(202).send({ status: 'error', message: 'Param cadena is required' });
        }
        
        let pila = []
        var valores = "[]()"
        for (var j = 0; j < cadena.length; j++) {
            posicion = valores.indexOf(cadena[j])
            if (posicion === -1) {
                break
            }
            if (posicion % 2 === 0) {
                pila.push(posicion + 1)
            } else {
                if (pila.length === 0 || pila.pop() !== posicion) {
                    return res.status(200).send({  cadena,status: false });
                }
            }
        }
        return res.status(200).send({ 
                cadena,
                status: pila.length === 0
             });

    }

};

