/**
 * Funcion4Controller
 *
 * @description ::Este ejercicio su complejidad fue baja, se tuvo que analizar el ejemplo de entrada y que salida . el tiempo aproximado fue de 20 min de desarrollo actualmente con javascript y ES se puede hacer en una sola linea pero para efecto tambien de esta prueba realice el eliminado de duplicados con un pequeño ciclo
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    duplicated: async function (req, res) {
        var values = req.allParams();
        let arreglo=values.arreglo
        console.log(arreglo)
        if (arreglo === undefined) {
            return res.status(202).send({ status: 'error', message: 'Param arreglo are required' });
        }
        if ( !Array.isArray(arreglo) ) {
            return res.status(202).send({ status: 'error', message: 'Param arreglo must be an array ' });
        }
        arreglo=arreglo.sort()

        let newArray=[ ...new Set(arreglo)]
        
        let newArray2=arreglo.filter((item,index)=>{
            return arreglo.indexOf(item)===index
        })

        let newArray3 = []
        let numberOld = null
        let position= 0
        for(i=0;i<arreglo.length;i++){
            if(arreglo[i]!=numberOld){
                newArray3[position]=arreglo[i]
                numberOld=arreglo[i]
                position++;
            }
        }

        return res.status(200).send({ status: 'success', 
                                      result1: newArray,
                                      result2:newArray2,
                                      result3:newArray3
                                    });
    }

};

