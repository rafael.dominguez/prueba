/**
 * Funcion2Controller
 *
 * @description :: Este ejercicio su complejidad fue baja, se tuvo que analizar el ejemplo de entrada y que salida asi como la formula proporcionada. el tiempo aproximado fue de 15 min de desarrollo
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    sum: async function (req, res) {
        var values = req.allParams();
        let numberN=values.n
        let numberM=values.m
        if (numberN === undefined || numberM === undefined) {
            return res.status(202).send({ status: 'error', message: 'Params n and m are required' });
        }
        if (numberM <0 || numberN <0) {
            return res.status(202).send({ status: 'error', message: 'Params n and m are  more than 0 ' });
        }
        let result=0;
        for(i=numberN;i<=numberM; i++){
            result+=i
        }
        return res.status(202).send({ status: 'success', message: result });
    }

};

