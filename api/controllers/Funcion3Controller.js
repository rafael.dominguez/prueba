/**
 * Funcion3Controller
 *
 * @description :: Este ejercicio su complejidad fue baja, se tuvo que analizar el ejemplo de entrada y que salida . el tiempo aproximado fue de 20 min de desarrollo
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    more: async function (req, res) {
        var values = req.allParams();
        let arreglo=values.arreglo
        let numberK=values.k
        console.log(arreglo)
        console.log(numberK)
        if (arreglo === undefined || numberK === undefined) {
            return res.status(202).send({ status: 'error', message: 'Params arreglo and k are required' });
        }
        if ( !Array.isArray(arreglo) ) {
            return res.status(202).send({ status: 'error', message: 'Param arreglo must be an array ' });
        }
        if ( !Number.isInteger(numberK) && numberK>=0 ) {
            return res.status(202).send({ status: 'error', message: 'Param k must be a number ' });
        }
        let newArray=arreglo.sort()//Se puede ocupar la funcion sort para ordenar o bien un metodo de la burbuja
        /*Se puede obtener el resultado obteniendo el total de elementos menos el numero k que seria la posicion deseada*/
        return res.status(200).send({ status: 'success', message: newArray[newArray.length-numberK]});
    }

};

